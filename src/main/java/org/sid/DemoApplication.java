package org.sid;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import org.sid.dao.ContactRepository;
import org.sid.entities.Contact;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoApplication implements CommandLineRunner {
	@Autowired
	private ContactRepository contactRepository;

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}

	@Override
	public void run(String... arg0) throws Exception {
		DateFormat df=new SimpleDateFormat("dd/MM/yyyy");
		contactRepository.save(new Contact("Abdellaoui", "Ameni", df.parse("04/12/1992"), "ameni.abdellaoui@esprit.tn", 50224892, "C:\\Users\\user\\Desktop\\Photo iphone\\IMG_0159.JPG"));
		contactRepository.save(new Contact("Belkhiria", "Mahdi", df.parse("24/04/1990"), "mehdi@esprit.tn", 53224892, "C:\\Users\\user\\Desktop\\Photo iphone\\IMG_0159.JPG"));
		contactRepository.save(new Contact("Dayeh", "Amina", df.parse("22/10/1990"), "amina@esprit.tn", 95600939, "C:\\Users\\user\\Desktop\\Photo iphone\\IMG_0159.JPG"));
		contactRepository.findAll().forEach(c->{
			System.out.println(c.getNom());
		});
		
		
	}
}
